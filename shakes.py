import json
import sys

from elasticsearch import Elasticsearch, helpers


class Shakesdb:
    INDEX = 'shakespeare'

    def __init__(self, elastic_addr):
        print(f"Shakesdb connecting to {elastic_addr}")
        self.client = Elasticsearch(hosts=elastic_addr)
        assert self.client.indices.exists(self.INDEX), 'Index does not exist; run setup.sh first'

    def get_line_count(self):
        # Count API
        res_count_api = self.client.count(index=self.INDEX)['count']

        # SQL API
        body = f'{{"query": "SELECT COUNT(*) as count FROM {self.INDEX}"}}'
        res_sql_api = self.client.sql.query(body=body, format="json")['rows'][0][0]

        x = helpers.scan(self.client, size=1000, index=self.INDEX)
        scan_api = 0
        for _ in x:
            scan_api += 1

        return {"count_api": res_count_api, "sql_api": res_sql_api, "scan_api": scan_api}

    def get_longest_plays(self):

        def get_using_query_dsl():
            query = {
                "size": 0,
                "aggregations": {
                    "plays": {
                        "terms": {"field": "play_name", "size": 3},
                    }
                }
            }
            res = self.client.search(index=self.INDEX, body=json.dumps(query))
            # print(json.dumps(res, indent=4))
            top = [x['key'] for x in res['aggregations']['plays']['buckets']]
            return top

        def get_from_sql_api():
            query = {
                "query": "SELECT play_name, count(*) as lines FROM shakespeare GROUP BY play_name ORDER BY lines DESC LIMIT 3"
            }
            top = [x[0] for x in self.client.sql.query(body=query, format="json")['rows']]
            return top

        return {"query_dsl_api": get_using_query_dsl(), "sql_api": get_from_sql_api()}

    def get_to_be_or_not_to_be(self):
        query = {
            "query": {
                "match": {
                    "text_entry": {
                        "query": "to be or not to be that is the question",
                        "operator": "AND"
                    }
                }
            }
        }
        result = self.client.search(index=self.INDEX, body=json.dumps(query))
        return result

    def get_top_talkative_characters(self):

        def get_using_query_dsl():
            query = {
                "size": 0,
                "aggregations": {
                    "plays": {
                        "terms": {"field": "speaker", "size": 3},
                    }
                }
            }
            res = self.client.search(index=self.INDEX, body=json.dumps(query))
            top = [x['key'] for x in res['aggregations']['plays']['buckets']]
            return top

        def get_from_sql_api():
            query = {
                "query": f"SELECT speaker, COUNT(*) AS count FROM {self.INDEX} GROUP BY speaker ORDER BY count DESC LIMIT 3"
            }
            top = [x[0] for x in self.client.sql.query(body=query, format="json")['rows']]
            return top

        return {"query_dsl_api": get_using_query_dsl(), "sql_api": get_from_sql_api()}

    def get_plays_for_character(self, speaker):

        def get_using_query_dsl():
            query = {
                "size": 0,
                "query": {
                    "bool": {
                        "filter": {
                            "term": {
                                "speaker": speaker
                            }
                        }
                    }
                },
                "aggregations": {
                    "plays": {
                        "terms": {
                            "field": "play_name",
                            "size": 100
                        }
                    }
                }
            }
            res = self.client.search(index=self.INDEX, body=json.dumps(query))
            top = [x['key'] for x in res['aggregations']['plays']['buckets']]
            return top

        return {"query_dsl_api": get_using_query_dsl()}


if __name__ == "__main__":
    # allow overriding default host address for running in a container
    elastic_addr = sys.argv[1] if sys.argv[1:] else None
    db = Shakesdb(elastic_addr)
    print(f"Task 1 - Line-count: {db.get_line_count()}")
    print(f"Task 2 - Longest plays: {db.get_longest_plays()}")
    print(f"Task 3 - Famous line: {db.get_to_be_or_not_to_be()}")
    print(f"Task 4 - Talkative characters: {db.get_top_talkative_characters()}")
    most_talkative = "GLOUCESTER"
    print(f"Task 5 - Plays featuring the most talkative character: {db.get_plays_for_character(most_talkative)}")


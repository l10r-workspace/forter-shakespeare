# README #

### What is this repository for? ###

Forter task #2 - Shakespeare's plays

### Prerequisites ###

1. Docker 19.03.0+

### How to set-up and run? ###

1. Clone to a convenient location

    `git clone git@bitbucket.org:l10r-workspace/forter-shakespeare.git`
    
1. Install, setup and run:

    `docker-compose up`

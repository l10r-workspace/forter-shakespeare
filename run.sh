#!/bin/bash


function main() {
  ELASTIC_HOST="es01:9200"
  setup ${ELASTIC_HOST}
  python shakes.py ${ELASTIC_HOST}
}

function setup() {
  ELASTIC_HOST=$1
  # setup - better be idempotent because this runs again when the container re-starts
  if [[ ! -f shakespeare.json ]]; then
    echo shakespeare.json does not exist - downloading it now...
    curl -O https://download.elastic.co/demos/kibana/gettingstarted/8.x/shakespeare.json;
  fi

  # wait for elastic to start
  while !(curl -s -f ${ELASTIC_HOST}); do
    echo "Waiting for elastic to respond on ${ELASTIC_HOST}"
    sleep 1
  done

  # only create the mapping and load data if the index does not yet exist
  if (curl -f --head -H "Content-Type: application/json" ${ELASTIC_HOST}/shakespeare); then
    echo Index already exists, skipping mapping and loading steps
  else
    echo Creating mapping...
    curl -X PUT  -H "Content-Type: application/json" -d '{  "mappings": {    "properties": {    "speaker": {"type": "keyword"},    "play_name": {"type": "keyword"},    "line_id": {"type": "integer"},    "speech_number": {"type": "integer"}    }  }}'\
     ${ELASTIC_HOST}/shakespeare

    echo Loading data...
    curl -X POST -H 'Content-Type: application/x-ndjson' "${ELASTIC_HOST}/shakespeare/_bulk?pretty" --data-binary @shakespeare.json
  fi
}

main